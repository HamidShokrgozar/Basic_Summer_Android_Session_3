package com.sematec.hamid.workout2.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sematec.hamid.workout2.Models.StudentsModel;
import com.sematec.hamid.workout2.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Hamid on 16/08/2017.
 */

public class ListViewAdapter extends BaseAdapter {

    Context mContext;
    List<StudentsModel> students;

    public ListViewAdapter(Context mContext, List<StudentsModel> students) {
        this.mContext = mContext;
        this.students = students;
    }

    @Override
    public int getCount() {
        return students.size();
    }

    @Override
    public Object getItem(int position) {
        return students.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View rowView = LayoutInflater.from(mContext).inflate(R.layout.listview_item, viewGroup, false);

        TextView studentName = (TextView) rowView.findViewById(R.id.listViewItemName);
        ImageView logo = (ImageView) rowView.findViewById(R.id.listViewItemLogo);

        studentName.setText(students.get(position).getName());

        Picasso.with(mContext).load(students.get(position).getLogo()).into(logo);



        return rowView;
    }
}
