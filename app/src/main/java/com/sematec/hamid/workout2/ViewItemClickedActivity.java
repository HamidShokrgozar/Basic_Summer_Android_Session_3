package com.sematec.hamid.workout2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class ViewItemClickedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_item_clicked);

        String itemnameclicked=getIntent().getStringExtra("clickedName");
        Toast.makeText(this, itemnameclicked, Toast.LENGTH_SHORT).show();

    }
}
