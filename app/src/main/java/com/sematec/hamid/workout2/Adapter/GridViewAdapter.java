package com.sematec.hamid.workout2.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sematec.hamid.workout2.Models.FoodModel;
import com.sematec.hamid.workout2.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Hamid on 16/08/2017.
 */

public class GridViewAdapter extends BaseAdapter {
    Context mContext;
    List<FoodModel> foods;

    public GridViewAdapter(Context mContext, List<FoodModel> foods) {
        this.mContext = mContext;
        this.foods = foods;
    }

    @Override
    public int getCount() {
        return foods.size();
    }

    @Override
    public Object getItem(int position) {
        return foods.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View rowView = LayoutInflater.from(mContext).inflate(R.layout.gridview_item, viewGroup, false);

        TextView foodName = (TextView) rowView.findViewById(R.id.gridViewItemName);
        ImageView logo = (ImageView) rowView.findViewById(R.id.gridViewItemLogo);

        foodName.setText(foods.get(position).getName());

        Picasso.with(mContext).load(foods.get(position).getLogo()).into(logo);

        return rowView;
    }
}
