package com.sematec.hamid.workout2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.sematec.hamid.workout2.Adapter.ListViewAdapter;
import com.sematec.hamid.workout2.Models.StudentsModel;

import java.util.ArrayList;
import java.util.List;

public class ListViewActivity extends AppCompatActivity {
    ListView studentsList;
    List<StudentsModel> students;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);


        studentsList = (ListView) findViewById(R.id.listView);

        StudentsModel student1 = new StudentsModel();
        student1.setName(getString(R.string.student1));
        student1.setLogo("https://gitlab.com/uploads/-/system/user/avatar/1521332/avatar.png");

        StudentsModel student2 = new StudentsModel();
        student2.setName(getString(R.string.student2));
        student2.setLogo("http://alipshirazi.persiangig.com/Ali/Ali%20Parvane%20Shirazi.jpg");

        StudentsModel student3 = new StudentsModel();
        student3.setName(getString(R.string.student3));
        student3.setLogo("http://physics.semnan.ac.ir/uploads/hagh_shenas.stu.semnan.ac.ir_17965.jpg");

        StudentsModel student4 = new StudentsModel();
        student4.setName(getString(R.string.student4));
        student4.setLogo("http://www.iranmodares.com/user/pic/1420.jpg");

        StudentsModel student5 = new StudentsModel();
        student5.setName(getString(R.string.student5));
        student5.setLogo("http://www.ahvazccim.com/old/dir/files/images/105-profile.jpg");

        StudentsModel student6 = new StudentsModel();
        student6.setName(getString(R.string.student6));
        student6.setLogo("http://s1.picofile.com/file/7509906234/Dr_Rostami.jpg");

        StudentsModel student7 = new StudentsModel();
        student7.setName(getString(R.string.student7));
        student7.setLogo("http://www.khstp.ir/Users/Content/Images/Managers/Manager-25.jpg");

        students= new ArrayList<>();
        students.add(student1);
        students.add(student2);
        students.add(student3);
        students.add(student4);
        students.add(student5);
        students.add(student6);
        students.add(student7);


        ListViewAdapter adapter = new ListViewAdapter(this, students);

        studentsList.setAdapter(adapter);

        studentsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                StudentsModel studentsModel = (StudentsModel) adapterView.getItemAtPosition(position);

                Intent intent =new Intent(ListViewActivity.this,ViewItemClickedActivity.class);
                intent.putExtra("clickedName",students.get(position).getName());
                startActivity(intent);

            }
        });

    }
}
