package com.sematec.hamid.workout2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.sematec.hamid.workout2.Adapter.GridViewAdapter;
import com.sematec.hamid.workout2.Models.FoodModel;

import java.util.ArrayList;
import java.util.List;

public class GridViewActivity extends AppCompatActivity {
    GridView foodList;
    List<FoodModel> foods;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view);

        foodList = (GridView) findViewById(R.id.gridView);

        FoodModel food1 = new FoodModel();
        food1.setName(getString(R.string.food1));
        food1.setLogo("http://uc.niksalehi.com/images/qw28m01lo2mdp4mt5t5.jpg");

        FoodModel food2 = new FoodModel();
        food2.setName(getString(R.string.food2));
        food2.setLogo("http://mepatogh.ir/wp-content/uploads/2016/10/1475410923-kabab-barg.jpg");

        FoodModel food3 = new FoodModel();
        food3.setName(getString(R.string.food3));
        food3.setLogo("http://gallery.avazak.ir/albums/userpics/10001/normal_Avazak_ir-Food%20%2818%29.jpg");

        FoodModel food4 = new FoodModel();
        food4.setName(getString(R.string.food4));
        food4.setLogo("http://gallery.avazak.ir/albums/userpics/10001/normal_Avazak_ir-Food%20%2831%29.jpg");

        FoodModel food5 = new FoodModel();
        food5.setName(getString(R.string.food5));
        food5.setLogo("http://gallery.avazak.ir/albums/userpics/10001/normal_Avazak_ir-Food%20(47).jpg");

        FoodModel food6 = new FoodModel();
        food6.setName(getString(R.string.food6));
        food6.setLogo("http://www.fundoon.ir/wp-content/uploads/2013/10/qaza-khoraki-11.jpg");

        FoodModel food7 = new FoodModel();
        food7.setName(getString(R.string.food7));
        food7.setLogo("http://wisgoon.com/media/pin/images/o/2013/11/1383571050233157.jpg");

        FoodModel food8 = new FoodModel();
        food8.setName(getString(R.string.food8));
        food8.setLogo("http://www.axgig.com/images/04784109408469583450.jpg");

        FoodModel food9 = new FoodModel();
        food9.setName(getString(R.string.food9));
        food9.setLogo("http://www.chasingtheunexpected.com/wp-content/uploads/2014/06/saffron-rice.jpg");

        FoodModel food10 = new FoodModel();
        food10.setName(getString(R.string.food10));
        food10.setLogo("http://www.fundoon.ir/wp-content/uploads/2013/10/qaza-khoraki-1.jpg");

        FoodModel food11 = new FoodModel();
        food11.setName(getString(R.string.food11));
        food11.setLogo("http://3pide.ir/wp-content/uploads/2015/04/Photo-by-eating-Arabs-Arabs-eat-gutty-images-3pide-3.jpg");

        FoodModel food12 = new FoodModel();
        food12.setName(getString(R.string.food12));
        food12.setLogo("http://aks.roshd.ir/photos/68.23631.medium.aspx");

        foods = new ArrayList<>();

        foods.add(food1);
        foods.add(food2);
        foods.add(food3);
        foods.add(food4);
        foods.add(food5);
        foods.add(food6);
        foods.add(food7);
        foods.add(food8);
        foods.add(food9);
        foods.add(food10);
        foods.add(food11);
        foods.add(food12);

        GridViewAdapter adapter = new GridViewAdapter(this, foods);

        foodList.setAdapter(adapter);

        foodList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                FoodModel foodModel = (FoodModel) adapterView.getItemAtPosition(position);

                Intent intent = new Intent(GridViewActivity.this, ViewItemClickedActivity.class);
                intent.putExtra("clickedName", foods.get(position).getName());
                startActivity(intent);

            }
        });

    }
}
