package com.sematec.hamid.workout2.Models;

/**
 * Created by Hamid on 16/08/2017.
 */

public class StudentsModel {

    String name;
    String logo;

    public StudentsModel() {
    }

    public StudentsModel(String name, String logo) {
        this.name = name;
        this.logo = logo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }


}
